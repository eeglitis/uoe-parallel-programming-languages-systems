# Parallel Programming Languages & Systems - 5th Year Informatics Coursework #

This repository contains two courseworks dealing with programs with some degree of parallelism.

* Coursework 1 (theory-based)
	* Determine all possible outcomes of a given parallel program
* Coursework 2 (written in C)
	* Implement the parallel prefix sum algorithm using Pthreads