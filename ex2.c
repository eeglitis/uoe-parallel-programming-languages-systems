// PPLS Exercise 2 Modified File

// ***** Implementing the Parallel Prefix Sum ***** //
//
// The first step in implementing the algorithm was to decide on the thread
// properties. Since each thread operates on its specific chunk, it must know
// the starting index of its chunk, and either the chunk end index or length.
// Additionally, the thread must have a pointer to the actual data array.
// In order to reuse the same simple loop structure used for the sequential sum,
// it was decided to use the chunk end index instead of its length. As a result,
// each thread required 3 parameters to be passed to it, which was implemented
// by using a struct as in the multiArgumentThreads.c example.
//
// The chunk start and end indices were obtained after determining the chunk size,
// which was calculated as floor(NITEMS/NTHREADS). For the last chunk, its size
// was variable, so its end index was set to NITEMS.
//
// The main algorithm functionality was split into dedicated functions for each
// of the three phases, with required synchronisation at the two boundaries.
// These are called by worker functions, with thread 0 having a separate one
// due to it executing phase 2 instead of the rest executing phase 3.
//
// * Phase 1 is carried out by all threads. The base computation is essentially
// the for-loop of the sequential sum, but delimited by the start and end indices.
//
// * Phase 1-2 synchronisation involves thread 0 waiting on all other threads
// to finish. This is tracked using a condition variable, which is set by the
// last thread to finish phase 1, and read by thread 0. As all threads are still
// in phase 1, in order to distinguish this setting/waiting behavior, thread 0
// uses a slightly different phase 1 function, which does not need to signal itself
// and does not wait if it is the last thread to finish phase 1. In order to track
// the number of threads finished, a counter variable is used, with its increment
// procedure accessible only through a lock to prevent false increments. 
//
// * Phase 2 is carried out only by thread 0. The base computation is once again
// like the sequential sum for-loop, but it iterates only through the chunk top
// elements by incrementing the loop counter by the chunk size. Thread 0 already
// knows this size, as it matches the end index of thread 0. Note that the top
// element of the last chunk is set after the loop due to its variable chunk size.
//
// * Phase 2-3 synchronisation involves thread 0 being waited on by all other
// threads. This is tracked using a second condition variable, which is set by
// thread 0 upon finishing phase 2, and read by all other threads. Since all other
// threads are still in their phase 1 function, this synchronisation can reuse
// the barrier of phase 1-2 synchronisation, only requiring to change the
// condition variable. As thread 0 uses its own phase 1 function, its behavior
// is not affected.
//
// * Phase 3 is carried out by all threads except thread 0. Each thread obtains 
// the top element of the previous chunk (at own start index, minus 1) and adds
// it to all (except the last) elements of its own chunk.
//
// Once thread 0 has completed phase 2, and all other threads have completed
// phase 3, no further work is needed, so all threads are joined, and their
// allocated memory freed.

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

// Print a helpful message followed by the contents of an array
// Controlled by the value of SHOWDATA, which should be defined
// at compile time. Useful for debugging.
void showdata (char *message,  int *data,  int n) {
  int i; 

if (SHOWDATA) {
    printf (message);
    for (i=0; i<n; i++ ){
     printf (" %d", data[i]);
    }
    printf("\n");
  }
}

// Check that the contents of two integer arrays of the same length are equal
// and return a C-style boolean
int checkresult (int* correctresult,  int *data,  int n) {
  int i; 

  for (i=0; i<n; i++ ){
    if (data[i] != correctresult[i]) return 0;
  }
  return 1;
}

// Compute the prefix sum of an array **in place** sequentially
void sequentialprefixsum (int *data, int n) {
  int i;

  for (i=1; i<n; i++ ) {
    data[i] = data[i] + data[i-1];
  }
}

// ***************************************************
// ******** PARALLEL PREFIX SUM SETUP - START ********
// ***************************************************

// Struct for thread arguments
typedef struct arg_struct {
  int *data; // Pointer to full data array
  int start; // Start index of chunk
  int end;   // Start index of next chunk (upper bound)
} arg_struct;

int phaseOneArrived = 0;         // counter for threads finished with Phase One
pthread_mutex_t phaseOneBarrier; // mutex to ensure counter is updated correctly
pthread_cond_t phaseTwoStart;    // condition variable for starting Phase Two
pthread_cond_t phaseThreeStart;  // condition variable for starting Phase Three


// Phase One behavior of Thread 0
void phaseonefirst(int *data, int end) {
  // Operation same as for sequential sum but with reduced length
  sequentialprefixsum(data, end);

  // Increment barrier counter
  pthread_mutex_lock(&phaseOneBarrier);
  phaseOneArrived++;

  // If other threads still working, wait for signal
  if (phaseOneArrived!=NTHREADS) {
    pthread_cond_wait(&phaseTwoStart, &phaseOneBarrier);
  }

  pthread_mutex_unlock(&phaseOneBarrier);
}


// Phase One behavior of all threads but Thread 0
void phaseonebase(int *data, int start, int end) {
  int i;
  for (i=start+1; i<end; i++) {
    data[i] += data[i-1];
  }

  // Increment barrier counter
  pthread_mutex_lock(&phaseOneBarrier);
  phaseOneArrived++;

  // If all threads done, signal Thread 0 to start Phase Two
  if (phaseOneArrived==NTHREADS) {
    pthread_cond_signal(&phaseTwoStart);
  }

  // Wait for Thread 0 to finish Phase Two
  pthread_cond_wait(&phaseThreeStart, &phaseOneBarrier);
  pthread_mutex_unlock(&phaseOneBarrier);
}


// Phase Two behavior of Thread 0
void phasetwo(int *data, int end) {
  int i, loopstart, loopend;

  loopstart = 2*end-1;          // highest index of chunk 1
  loopend   = (NTHREADS-1)*end; // lowest index of last chunk (chunk NTHREADS)

  // Change highest index for threads 1 through NTHREADS-1
  for (i=loopstart; i<loopend; i+=end) {
    data[i] += data[i-end];
  }

  // Change highest index for thread NTHREADS
  data[NITEMS-1] += data[loopend-1];

  // Signal other threads to begin Phase Three
  pthread_cond_broadcast(&phaseThreeStart);
}


// Phase Three behavior of all threads but Thread 0
void phasethree(int *data, int start, int end) {
  int i, prevmax;

  prevmax = data[start-1]; // top element of previous chunk

  for (i=start; i<end-1; i++) {
    data[i] += prevmax;
  }
}


// Full behavior of Thread 0, for Phases One and Two.
// The start argument is not extracted as it is always 0
void *FirstWorker (void *arg) {
  int *data = ((arg_struct *) arg)->data;
  int end   = ((arg_struct *) arg)->end;

  phaseonefirst(data, end);
  phasetwo(data, end);
}


// Full behavior of all threads but Thread 0, for Phases One and Three
void *BaseWorker (void *arg) {
  int *data = ((arg_struct *) arg)->data;
  int start = ((arg_struct *) arg)->start;
  int end   = ((arg_struct *) arg)->end;

  phaseonebase(data, start, end);
  phasethree(data, start, end);
}


// YOU MUST WRITE THIS FUNCTION AND ANY ADDITIONAL FUNCTIONS YOU NEED
void parallelprefixsum (int *data, int n) {
  int chunk, i;

  // Initialise mutex and condition variables
  pthread_mutex_init(&phaseOneBarrier, NULL);
  pthread_cond_init(&phaseTwoStart, NULL);
  pthread_cond_init(&phaseThreeStart, NULL);

  // Set up containers for threads and their arguments
  pthread_t  *threads;
  arg_struct *threadargs;
  threads    = (pthread_t *)  malloc(NTHREADS * sizeof(pthread_t));
  threadargs = (arg_struct *) malloc(NTHREADS * sizeof(arg_struct));

  // Get average chunk size: casting to int acts as the floor function
  chunk =  n/NTHREADS; 

  // Set up arguments for all threads (last thread iterates until end of array)
  for (i=0; i<NTHREADS; i++) {
    threadargs[i].data  = data;
    threadargs[i].start = i*chunk;
    threadargs[i].end   = (i+1)*chunk;
  }
  threadargs[NTHREADS-1].end = n;
  
  // Initialise all threads
  pthread_create(&threads[0], PTHREAD_CREATE_JOINABLE, FirstWorker, (void *) &threadargs[0]);
  for (i=1; i<NTHREADS; i++) {
    pthread_create(&threads[i], PTHREAD_CREATE_JOINABLE, BaseWorker, (void *) &threadargs[i]);
  }

  // Wait for all threads to finish
  for (i=0; i<NTHREADS; i++) {
    pthread_join(threads[i], NULL);
  }

  // Free up memory
  free(threads);
  free(threadargs);
}

// *************************************************
// ******** PARALLEL PREFIX SUM SETUP - END ********
// *************************************************

int main (int argc, char* argv[]) {

  int *arr1, *arr2, i;

  // Check that the compile time constants are sensible for this exercise
  if ((NITEMS>10000000) || (NTHREADS>32)) {
    printf ("So much data or so many threads may not be a good idea! .... exiting\n");
    exit(EXIT_FAILURE);
  }

  // Create two copies of some random data
  arr1 = (int *) malloc(NITEMS*sizeof(int));
  arr2 = (int *) malloc(NITEMS*sizeof(int));
  srand((int)time(NULL));
  for (i=0; i<NITEMS; i++) {
     arr1[i] = arr2[i] = rand()%5;
  }
  showdata ("initial data          : ", arr1, NITEMS);

  // Calculate prefix sum sequentially, to check against later
  sequentialprefixsum (arr1, NITEMS);
  showdata ("sequential prefix sum : ", arr1, NITEMS);

  // Calculate prefix sum in parallel on the other copy of the original data
  parallelprefixsum (arr2, NITEMS);
  showdata ("parallel prefix sum   : ", arr2, NITEMS);

  // Check that the sequential and parallel results match
  if (checkresult(arr1, arr2, NITEMS))  {
    printf("Well done, the sequential and parallel prefix sum arrays match.\n");
  } else {
    printf("Error: The sequential and parallel prefix sum arrays don't match.\n");
  }

  return 0;
}
